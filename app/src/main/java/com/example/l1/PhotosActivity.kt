package com.example.l1

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.example.l1.adapters.PhotoAdapter
import kotlinx.android.synthetic.main.activity_photos.*
import com.example.l1.database.AppDatabase
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import com.example.l1.database.entities.Photo

class PhotosActivity : AppCompatActivity() {

    companion object {

        private val KEY_MARKER_ID = "keyMarkerUUID"
        private val KEY_MARKER_URIS_TO_ADD = "keyMarkerURIs"

        val EXTRA_REPLY_MARKER_ID = "extraReplyMarkerID"
        val EXTRA_REPLY_URIS_TO_ADD = "extraReplyURIsToAdd"
    }

    private var mPhotoAdapter: PhotoAdapter? = null

    private val urisToAdd = mutableListOf<String>()

    private var markerId : Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_photos)
        setSupportActionBar(toolbar)

        if (savedInstanceState != null) {
            markerId = savedInstanceState.getLong(KEY_MARKER_ID)
            val uris = savedInstanceState.getStringArray(KEY_MARKER_URIS_TO_ADD)
            if (uris != null) {
                urisToAdd.addAll(uris)
            }
        } else {
            markerId = intent.extras.getLong(MapsActivity.EXTRA_MARKER_ID)
        }

        if (markerId == null) {
            setResult(Activity.RESULT_CANCELED, intent)
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = markerId.toString()

        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
        mPhotoAdapter = PhotoAdapter(this)
        recyclerView.adapter = mPhotoAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)

        setupPhotos()
        setupListeners()
    }

    private fun setupListeners() {
        btnAdd.setOnClickListener {
            PickImageDialog.build(PickSetup())
                .setOnPickResult { pickResult ->
                    urisToAdd.add(pickResult.path)
                    setupPhotos()
                }.show(supportFragmentManager)
        }

        btnOK.setOnClickListener {
            val intent = Intent()
            if (markerId != null) {
                intent.putExtra(EXTRA_REPLY_MARKER_ID, markerId!!)
                intent.putExtra(EXTRA_REPLY_URIS_TO_ADD, urisToAdd.toTypedArray())
                setResult(Activity.RESULT_OK, intent)
            } else {
                setResult(Activity.RESULT_CANCELED, intent)
            }
            finish()
        }

        btnCancel.setOnClickListener {
            setResult(Activity.RESULT_CANCELED, intent)
            finish()
        }
    }

    private fun setupPhotos() {

        doAsync {
            val database = AppDatabase.getInstance(context = this@PhotosActivity)
            val photosFromDb = database.photoDao().getAllByMarkerId(markerId!!)
            var photosToAdd = urisToAdd.map { uri -> Photo(markerId!!, uri)}

            uiThread {
                mPhotoAdapter!!.setPhotos(photosFromDb.union(photosToAdd).toList())
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (outState != null) {
            if (markerId != null) {
                outState.putLong(KEY_MARKER_ID, markerId!!)
            }
            outState.putStringArray(KEY_MARKER_URIS_TO_ADD, urisToAdd.toTypedArray())
        }
    }
}
