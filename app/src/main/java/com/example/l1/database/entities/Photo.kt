package com.example.l1.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "photos",
    foreignKeys = [
        ForeignKey(
            entity = Marker::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("marker_id"),
            onDelete = ForeignKey.CASCADE
        )
    ])
class Photo constructor (markerId: Long, uri: String) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    @ColumnInfo(name = "marker_id")
    var markerId: Long = markerId

    var uri: String = uri
}