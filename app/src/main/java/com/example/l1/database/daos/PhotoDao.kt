package com.example.l1.database.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.l1.database.entities.Photo

@Dao
interface PhotoDao {

    @Insert
    fun insert(photo: Photo): Long

    @Insert
    fun insertAll(photos: List<Photo>): List<Long>

    @Delete
    fun delete(photo: Photo)

    @Query("DELETE FROM photos")
    fun deleteAll()

    @Query("SELECT * from photos")
    fun getAll(): List<Photo>

    @Query("SELECT * from photos where marker_id = :markerId")
    fun getAllByMarkerId(markerId: Long): List<Photo>
}