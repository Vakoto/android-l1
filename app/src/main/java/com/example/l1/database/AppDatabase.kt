package com.example.l1.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.l1.database.daos.MarkerDao
import com.example.l1.database.daos.PhotoDao
import com.example.l1.database.entities.Marker
import com.example.l1.database.entities.Photo

@Database(entities = [(Marker::class), (Photo::class)], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun markerDao(): MarkerDao

    abstract fun photoDao(): PhotoDao

    companion object {

        @Volatile private var INSTANCE: AppDatabase? = null

        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room
                            .databaseBuilder(
                                context.applicationContext,
                                AppDatabase::class.java,
                                "map_database"
                            )
                            .build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}