package com.example.l1.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "markers")
class Marker constructor(latitude: Double, longitude: Double) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    var latitude: Double = latitude

    var longitude: Double = longitude
}