package com.example.l1.database.daos

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.example.l1.database.entities.Marker

@Dao
interface MarkerDao {

    @Insert
    fun insert(marker: Marker): Long

    @Insert
    fun insertAll(markers: List<Marker>): List<Long>

    @Delete
    fun delete(marker: Marker)

    @Query("DELETE FROM markers")
    fun deleteAll()

    @Query("SELECT * from markers")
    fun getAll(): List<Marker>

    @Query("SELECT * from markers where id = :markerId LIMIT 1")
    fun getById(markerId: Long): Marker

    @Query("SELECT * FROM markers WHERE id IN (:markersId)")
    fun getAllByIds(markersId: Array<Long>): List<Marker>
}