package com.example.l1.adapters

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.l1.R
import com.example.l1.database.entities.Photo
import kotlinx.android.synthetic.main.recyclerview_photos_item.view.*
import java.util.*

class PhotoAdapter: RecyclerView.Adapter<PhotoAdapter.PhotoViewHolder> {

    inner class PhotoViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemView: ImageView = itemView.findViewById(R.id.itemImage)
    }

    private var mInflater: LayoutInflater

    private var mPhotos = Collections.emptyList<Photo>()

    constructor(context: Context) {
        mInflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        val itemView = mInflater.inflate(R.layout.recyclerview_photos_item, parent, false)
        return PhotoViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val current = mPhotos[position]
        holder.itemView.itemImage.setImageURI(Uri.parse(current.uri))
    }

    fun setPhotos(photos: List<Photo>) {
        mPhotos = photos
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return mPhotos.size
    }
}