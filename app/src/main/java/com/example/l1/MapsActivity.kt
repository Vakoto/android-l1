package com.example.l1

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import android.app.Activity
import android.widget.Toast
import com.androidmapsextensions.GoogleMap
import com.androidmapsextensions.MarkerOptions
import com.androidmapsextensions.OnMapReadyCallback
import com.androidmapsextensions.SupportMapFragment
import com.example.l1.database.AppDatabase
import com.example.l1.database.entities.Marker
import com.example.l1.database.entities.Photo
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    companion object {

        private val EDIT_PHOTO_LIST_REQUEST_CODE = 1

        val EXTRA_MARKER_ID = "markerMetaExtra"
    }

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setupMapIfNeeded()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        loadMapState()
        setupListeners()
    }

    private fun setupListeners() {
        mMap.setOnMapClickListener { pos: LatLng ->

            doAsync {
                val marker = Marker(pos.latitude, pos.longitude)
                val database = AppDatabase.getInstance(context = this@MapsActivity)
                val id = database.markerDao().insert(marker)

                uiThread {
                    mMap.addMarker(
                        MarkerOptions()
                            .position(LatLng(marker.latitude, marker.longitude))
                            .data(id)
                    )
                }
            }
        }

        mMap.setOnMarkerClickListener { marker ->
            val id = marker.getData<Long>()
            val intent = Intent(this@MapsActivity, PhotosActivity::class.java)
            intent.putExtra(EXTRA_MARKER_ID, id)
            startActivityForResult(intent, EDIT_PHOTO_LIST_REQUEST_CODE)
            false
        }
    }

    private fun loadMapState() {
        val mpm = MapPreferencesManager(this)
        val position = mpm.savedCameraPosition
        if (position != null) {
            val update = CameraUpdateFactory.newCameraPosition(position)
            mMap.moveCamera(update)
            mMap.mapType = mpm.savedMapType
        }

        doAsync {
            val database = AppDatabase.getInstance(context = this@MapsActivity)
            val markers = database.markerDao().getAll()

            uiThread {
                for (marker in markers) {
                    mMap.addMarker(
                        MarkerOptions()
                            .position(LatLng(marker.latitude, marker.longitude))
                            .data(marker.id)
                    )
                }
            }
        }
    }

    private fun setupMapIfNeeded() {
        if (!::mMap.isInitialized) {
            val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getExtendedMapAsync(this)
        }
    }

    override fun onPause() {
        super.onPause()
        val mpm = MapPreferencesManager(this)
        mpm.saveMapPreferences(mMap)
    }

    override fun onResume() {
        super.onResume()
        setupMapIfNeeded()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDIT_PHOTO_LIST_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {

                val markerId = data!!.extras.getLong(PhotosActivity.EXTRA_REPLY_MARKER_ID)
                val uris = data!!.extras.getStringArray(PhotosActivity.EXTRA_REPLY_URIS_TO_ADD)

                if (uris != null) {
                    doAsync {
                        val photos = uris.map { uri -> Photo(markerId, uri) }
                        AppDatabase.getInstance(context = this@MapsActivity).photoDao().insertAll(photos)
                    }
                }
            } else {
                Toast.makeText(applicationContext, R.string.changes_not_saved, Toast.LENGTH_LONG).show()
            }
        }
    }
}