package com.example.l1

import android.content.Context
import android.content.SharedPreferences
import com.androidmapsextensions.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng

class MapPreferencesManager(context: Context) {

    companion object {

        private val MAP_TYPE = "mapType"
        private val PREFS_NAME = "mapPrefsState"
        private val CAMERA_LONGITUDE = "cameraLongitude"
        private val CAMERA_LATITUDE = "cameraLatitude"
        private val CAMERA_ZOOM = "cameraZoom"
        private val CAMERA_BEARING = "cameraBearing"
        private val CAMERA_TILT = "cameraTilt"
    }

    private val mapPreferences: SharedPreferences

    init {
        mapPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
    }

    val savedCameraPosition: CameraPosition?
        get() {
            val latitude = mapPreferences.getFloat(CAMERA_LATITUDE, 0f).toDouble()
            if (latitude == 0.0) {
                return null
            }
            val longitude = mapPreferences.getFloat(CAMERA_LONGITUDE, 0f).toDouble()
            val target = LatLng(latitude, longitude)

            val zoom = mapPreferences.getFloat(CAMERA_ZOOM, 0f)
            val bearing = mapPreferences.getFloat(CAMERA_BEARING, 0f)
            val tilt = mapPreferences.getFloat(CAMERA_TILT, 0f)

            return CameraPosition(target, zoom, tilt, bearing)
        }

    val savedMapType: Int
        get() = mapPreferences.getInt(MAP_TYPE, GoogleMap.MAP_TYPE_NORMAL)

    fun saveMapPreferences(map: GoogleMap) {
        val editor = mapPreferences.edit()

        editor.putInt(MAP_TYPE, map.mapType)

        val cameraPosition = map.cameraPosition
        editor.putFloat(CAMERA_LATITUDE, cameraPosition.target.latitude.toFloat())
        editor.putFloat(CAMERA_LONGITUDE, cameraPosition.target.longitude.toFloat())
        editor.putFloat(CAMERA_ZOOM, cameraPosition.zoom)
        editor.putFloat(CAMERA_TILT, cameraPosition.tilt)
        editor.putFloat(CAMERA_BEARING, cameraPosition.bearing)

        editor.commit()
    }
}